# Invoicetestapp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.7.

## Install package

Run `npm install` for a download package

## Development server

Run `ng serve` or `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Login and password
Login `admin@admin.com`
Password `admin`
