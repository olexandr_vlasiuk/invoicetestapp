import { Component, OnInit } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

import { InvoiceService } from '../services/invoice.service';

@Component({
  selector: 'app-invoice-page',
  templateUrl: './invoice-page.component.html',
  styleUrls: ['./invoice-page.component.css']
})

export class InvoicePageComponent implements OnInit {

  searchControl = new FormControl();
  searchString: string = '';

  constructor(private invoiceService: InvoiceService) { }

  ngOnInit() {
    this.searchControl.valueChanges
      .pipe(debounceTime(300))
      .subscribe((newValue: string) => {
        this.searchString = newValue;
      });
  }
}
