import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';

import { LoginService } from './services/login.service';


@Injectable()
export class CheckAccess implements CanActivate {

  constructor(private loginService: LoginService,
    private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.loginService.checkAuthData()) {
      return true;
    }
    this.router.navigate(['login']);
    return false;
  }
}
