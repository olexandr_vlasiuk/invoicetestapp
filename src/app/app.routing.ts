import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginPageComponent } from './login-page/login-page.component';
import { InvoicePageComponent } from './invoice-page/invoice-page.component';

import { CheckAccess } from './checkAccess';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginPageComponent, pathMatch: 'full' },
  { path: 'invoice-list', component: InvoicePageComponent, pathMatch: 'full', canActivate: [CheckAccess] }
];

@NgModule({
    imports : [ RouterModule.forRoot( routes ) ] ,
    exports : [ RouterModule ]
})

export class AppRouter { }
