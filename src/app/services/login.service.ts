import { Injectable, Inject } from '@angular/core';
import { SESSION_STORAGE, WebStorageService } from 'angular-webstorage-service';
import { Router } from '@angular/router';

@Injectable()
export class LoginService {

  constructor(
    @Inject(SESSION_STORAGE) private storage: WebStorageService,
    private router: Router) { }

  defaultAuth = {
    email: 'admin@admin.com',
    password: 'admin'
  };

  login = (em:string , pas: string) => {
    const { password, email } = this.defaultAuth;
    if (password === pas && email === em) {
      this.storage.set('authData', { email, password });
      this.router.navigate(['invoice-list']);
    } else {
      alert('Wrong authorization data!');
    }
  }

  checkAuthData = ():any =>
     this.storage.get('authData');

}
