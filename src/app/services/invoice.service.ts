import { Injectable } from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';

import { map, scan, publishReplay, combineLatest, refCount } from 'rxjs/operators';

import { Invoice } from '../models/invoice.model';

type Invoices = (invoices: Invoice[]) => Invoice[];
const initialInvoice: Invoice[] = JSON.parse(localStorage.getItem('invoice-list')) || [];

@Injectable()
export class InvoiceService {

  isAddModalOpen = false;
  invoices$: Observable<any>;
  create$: Subject<Invoice> = new Subject<Invoice>();
  update$: BehaviorSubject<Invoices> = new BehaviorSubject<Invoices>((invoice: Invoice[]) => invoice);
  dublicate$: Subject<{ invoice: Invoice; index: number; }> = new Subject<{ invoice: Invoice; index: number; }>();
  remove$: Subject<string> = new Subject<string>();

  constructor() {
    this.invoices$ = this.update$.pipe(
      scan((invoices: Invoice[], operation: Invoices) => operation(invoices), initialInvoice)
      , publishReplay(1), refCount());

    this.invoices$.forEach((invoices: Invoice[]) => localStorage.setItem('invoice-list', JSON.stringify(invoices)));

    this.create$.pipe(map((invoice: Invoice): Invoices => {
      return (invoices: Invoice[]) => {
        return invoices.concat(invoice);
      };
    })).subscribe(this.update$);

    this.dublicate$
      .pipe(map(({ invoice, index }) => {
        return (invoices: Invoice[]) => {
          invoices.splice(index, 0, invoice);
          return invoices;
        };
      }))
      .subscribe(this.update$);

    this.remove$
      .pipe(map((uuid: string): Invoices =>
      (invoices: Invoice[]) =>
      invoices.filter(invoice => invoice.id !== uuid)))
      .subscribe(this.update$);
  }

  toggleAddModal = (): void => {
    this.isAddModalOpen = !this.isAddModalOpen;
  } 

  addInvoice = (title: string): void =>
    this.create$.next(new Invoice(title));

  dublicateInvoice = (title: string, index: number): void => {
    const invoice = new Invoice(title);
    this.dublicate$.next({ invoice, index });
  }

  remove = (uuid: string): void =>
    this.remove$.next(uuid);
}
