import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StorageServiceModule } from 'angular-webstorage-service';

import { AppRouter } from './app.routing';

import { AppComponent } from './app.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { InvoicePageComponent } from './invoice-page/invoice-page.component';
import { InvoiceModalComponent } from './invoice-modal/invoice-modal.component';
import { InvoiceTableComponent } from './invoice-table/invoice-table.component';
import { InvoiceRowComponent } from './invoice-row/invoice-row.component';

import { LoginService } from './services/login.service';
import { InvoiceService } from './services/invoice.service';

import { CheckAccess } from './checkAccess';

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    InvoicePageComponent,
    InvoiceModalComponent,
    InvoiceTableComponent,
    InvoiceRowComponent
  ],
  imports: [
    BrowserModule,
    AppRouter,
    FormsModule,
    ReactiveFormsModule,
    StorageServiceModule
  ],
  providers: [
    LoginService,
    InvoiceService,
    CheckAccess
],
  bootstrap: [AppComponent]
})
export class AppModule { }
