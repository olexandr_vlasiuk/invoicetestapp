import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})

export class LoginPageComponent implements OnInit {

  email = 'admin@admin.com';
  password = 'admin';

  constructor(public loginService: LoginService) { }

  ngOnInit() { }
}
